#!/bin/bash

# This script creates or updates the SSM Parameters required for this project.
# This should be run whenever there are new parameters, updates to parameters, 
# or the project is being deployed in a new AWS account.

ENVIRONMENT=$1
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source $SCRIPTPATH/env.sh

put_ssm_param () {
    PARAMNAME=$1
    PARAMVALUE=$2
    EXISTING_VALUE=$(get_ssm_param $PARAMNAME)
    if [[ $EXISTING_VALUE == *"ParameterNotFound"* ]]; then
        SSM_SET_RESULT=$(aws ssm put-parameter --name $PARAMNAME --value "$PARAMVALUE" --type "SecureString" 2>&1)
        echo "✅ SSM Parameter $PARAMNAME Set to $PARAMVALUE"
    else
        # If the new value is different from the old value, prompt for overwrite
        if [[ "$EXISTING_VALUE" != "$PARAMVALUE" ]]; then
            echo ""
            echo "⚠️ $PARAMNAME"
            echo "  Current Value: $EXISTING_VALUE"
            echo "  New Value: $PARAMVALUE"
            read -r -p "Overwrite ? " REPLY
            if [[ $REPLY =~ ^[Yy] ]]; then
                SSM_SET_RESULT=$(aws ssm put-parameter --name $PARAMNAME --value "$PARAMVALUE" --type "SecureString" --overwrite 2>&1)
                echo "✅ SSM Parameter $PARAMNAME Set to $PARAMVALUE"
            else
                echo "❎ SSM Parameter $PARAMNAME unchanged"
            fi
        else
            echo "❎ SSM Parameter $PARAMNAME unchanged"
        fi
    fi
}

put_ssm_param /$SMS_PREFIX/api_private_key "$API_PRIVATE_KEY"
put_ssm_param /$SMS_PREFIX/api_public_key "$API_PUBLIC_KEY"
