#!/bin/bash -e

# Mount this directory in the container, then run the in-container-setup.sh script from within the container.
# docker run -it -rm --name pythonsetup -v `pwd`:/local --entrypoint /local/bin/in-container-setup.sh lambci/lambda:python3.7

ENVIRONMENT=$1
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
BASE="$SCRIPTPATH/.."

cd "$BASE/src/serverless/"

npm install

cd "$BASE"
