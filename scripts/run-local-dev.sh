#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source $SCRIPTPATH/env.sh
BASE="$SCRIPTPATH/.."
VUEPATH="$BASE/src/vue/serverless-webauth"

cd "$VUEPATH"

# Create a .env file
cat <<EOT > .env
VUE_APP_SERVICE_URL="$SERVICE_URL"
VUE_APP_WEBAUTH_LOGIN_URL="$WEBAUTH_LOGIN_URL"
VUE_APP_WEBAUTH_VALIDATE_URL="$WEBAUTH_VALIDATE_URL"
VUE_APP_API_URL="$API_URL"
EOT


npx vue-cli-service serve \
    --no-clean \
    --dest "$VUEPATH/dist" \
    --port 8081 \
    -v

VUEPID=$!

cd $BASE
