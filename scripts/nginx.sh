#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

docker run \
    --name nginx \
    -it \
    --rm \
    -p 0.0.0.0:443:443 \
    -v $(PWD)/src/vue/serverless-webauth/dist/:/usr/share/nginx/html/ \
    -v $(PWD)/nginx/nginx.conf:/etc/nginx/nginx.conf \
    -v $(PWD)/nginx/local_dev_arizona_edu.cer:/etc/nginx/local_dev_arizona_edu.cer \
    -v $(PWD)/nginx/local_dev_arizona_edu.key:/etc/nginx/local_dev_arizona_edu.key \
    nginx $1
