#!/bin/bash

# Force a re-deployment of the ECS service so that it picks up the latest docker image

ENVIRONMENT=$1
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
BASE="$SCRIPTPATH/.."
source $SCRIPTPATH/env.sh

cd "$BASE/src/serverless/"

sls deploy

cd "$BASE"
