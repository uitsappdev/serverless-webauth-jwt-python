UArizona Serverless Starter Project
===================================

This project is an attempt to serve as a starting point for developing secure web applications that authenticate through the UArizona CAS SSO service (WebAuth). This project uses the following basic services and techniques:

* JavaScript Front End Framework (Vue)
* AWS Serverless Backend (API Gateway, Lambda, DynamoDB)
* UArizona WebAuth CAS Protocol
* [JSON Web Tokens][jwt] for session AuthN / AuthZ]
* [Serverless Framework][sls]
* [AWS CloudFormation][cfn]
* [SSM Parameter Store][ssm]

[jwt]: https://jwt.io
[sls]: https://www.serverless.com
[cnf]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html
[ssm]: https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html

# Pre-Requisites

* [Serverless CLI][sls-install]
* AWS Account and active CLI credentials

[sls-install]: https://www.serverless.com/framework/docs/getting-started/
