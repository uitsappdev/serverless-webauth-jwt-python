import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// import auth from './auth'
import Home from './components/Home.vue'
import Auth from './components/Auth.vue'
import Todo from './components/Todo.vue'
// const Home = { template: '<div>home</div>' }
// const Foo = { template: '<div>foo</div>' }

Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Home },
    { path: '/auth', component: Auth },
    { path: '/todo', component: Todo }
  ]
})

new Vue({
  router
}).$mount('#app')
