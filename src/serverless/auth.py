import os
import sys
import logging
import json
import base64
import boto3
import jwt
import time
import pytz
from datetime import datetime, timedelta, timezone

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, datefmt='')
logger = logging.getLogger()

ssm_client = boto3.client('ssm')


def get_ssm_param(key, secret=False):
    resp = ssm_client.get_parameter(
        Name=key,
        WithDecryption=secret
    )

    return resp['Parameter']['Value']


def log_error(err_msg, event=None, context=None, tb=''):
    err_json = {
        'msg': err_msg,
        'event': event,
        'traceback': tb
    }
    json_str = json.dumps(err_json)
    logger.error(json_str)


if os.getenv("SSM_PREFIX"):
    ssm_prefix = os.getenv("SSM_PREFIX")
else:
    log_error("Environment Var SSM_PREFIX Not Set")

api_public_key = get_ssm_param(f"/{ssm_prefix}/api_public_key", True)
if api_public_key is None:
    logger.error(f"/{ssm_prefix}/api_public_key SSM parameter not found")

claims = None
username = None

def handler(event, context):
    global claims
    global username
    # utils.loginfo(event)

    if "requestId" in event['requestContext']:
        request_id = event['requestContext']['requestId']

    if "requestContext" in event:
        requestContext = event['requestContext']
        principal_id = None
        if "authorizer" in requestContext:
            principal_id = requestContext['authorizer']['principalId']


    auth_token = None
    if "Authorization" in event['headers']:
        auth_token = event['headers']["Authorization"]
    elif "authorization" in event['headers']:
        auth_token = event['headers']["authorization"]

    if auth_token is None:
        # Send back an unauthorized error
        log_error(f"Authorization not found in headers", event, context)
        return generate_response("Deny")

    (type, auth_encoded) = auth_token.split(" ")
    type = type.lower()

    if type != "bearer":
        log_error(f"Incorrect auth type found in Authorization Header: '{type}'", event, context)
        return generate_response("Deny")

    auth_encoded = auth_encoded.encode('utf-8')
    
    try:
        claims = jwt.decode(auth_encoded, api_public_key, algorithms='RS256')
    except jwt.ExpiredSignatureError:
        decoded = jwt.decode(auth_encoded, api_public_key, algorithms='RS256', verify=False)
        exp_time_utc = datetime.fromtimestamp(decoded['exp'], tz=timezone.utc)
        tz_arizona = pytz.timezone('America/Phoenix')
        fmt = '%Y-%m-%d %H:%M:%S %Z%z'
        exp_time_local = exp_time_utc.astimezone(tz_arizona)
        exp_time = exp_time_local.strftime(fmt)
        log_error(f"token expired {exp_time}", event, context)
        return generate_response("Deny")
    except jwt.InvalidSignatureError:
        decoded = jwt.decode(auth_encoded, api_public_key, algorithms='RS256', verify=False)
        exp_time_utc = datetime.fromtimestamp(decoded['exp'], tz=timezone.utc)
        tz_arizona = pytz.timezone('America/Phoenix')
        fmt = '%Y-%m-%d %H:%M:%S %Z%z'
        exp_time_local = exp_time_utc.astimezone(tz_arizona)
        exp_time = exp_time_local.strftime(fmt)
        log_error(f"Validation Failed {exp_time}", event, context)
        log_error(decoded)
        return generate_response("Deny")
    except Exception as ex:
        log_error(f"JWT Decode Failed", event, context)
        return generate_response("Deny")

    # Check for JWT expiration
    exp_time_utc = datetime.fromtimestamp(claims['exp'], tz=timezone.utc)
    #
    # TODO
    #

    if "uid" not in claims.keys():
        log_error(f"Missing claim uid", event, context)
        return generate_response("Deny")

    username = claims['uid']

    if "emplid" not in claims.keys():
        log_error(f"Missing claim emplid", event, context)
        return generate_response("Deny")


    return generate_response("Allow")


def generate_response(effect):
    if effect != "Allow" and effect != "Deny":
        log_error(f"Incorrect effect, defaulting to Deny: '{effect}'")

    auth_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "execute-api:Invoke",
                "Effect": effect,
                "Resource": "*"
            }
        ]
    }

    auth_response = {
        "principalId": username,
        "policyDocument": auth_policy
    }

    return auth_response
