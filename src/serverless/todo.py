import logging
import os
import json
import traceback
import sys
import requests
import boto3
import xml.etree.ElementTree as ET
import jwt
import time
import pytz
from datetime import datetime, timedelta, timezone

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, datefmt='')
logger = logging.getLogger()


def log_error(err_msg, event=None, context=None, tb=''):
    err_json = {
        'msg': err_msg,
        'event': event,
        'traceback': tb
    }
    json_str = json.dumps(err_json)
    logger.error(json_str)


def _format_response(message, code=200, **additional_fields):
    response = {
        'message': message
    }

    response.update(additional_fields)

    json_text = json.dumps(response)

    proxy_response = {
        'statusCode': code,
        'headers': {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key"
        },
        'body': json_text
    }
    return proxy_response


def handler(event, context):
    
    log_error(event)

    todo_list = [
        { 'id': 1, 'name': "Todo Item 1", 'done': False},
        { 'id': 2, 'name': "Todo Item 2", 'done': False},
        { 'id': 3, 'name': "Todo Item 3", 'done': True},
        { 'id': 4, 'name': "Todo Item 4", 'done': False}
    ]

    return _format_response(
        "Success",
        todos=todo_list
    )
