import logging
import os
import json
import traceback
import sys
import requests
import boto3
import xml.etree.ElementTree as ET
import jwt
import time
import pytz
from datetime import datetime, timedelta, timezone

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, datefmt='')
logger = logging.getLogger()

ssm_client = boto3.client('ssm')

def get_ssm_param(key, secret=False):
    resp = ssm_client.get_parameter(
        Name=key,
        WithDecryption=secret
    )

    return resp['Parameter']['Value']


def log_error(err_msg, event, context, tb=''):
    err_json = {
        'msg': err_msg,
        'event': event,
        'traceback': tb
    }
    json_str = json.dumps(err_json)
    logger.error(json_str)


def _format_response(message, code=200, **additional_fields):
    response = {
        'message': message
    }

    response.update(additional_fields)

    json_text = json.dumps(response)

    proxy_response = {
        'statusCode': code,
        'headers': {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,POST,OPTIONS",
            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key"
        },
        'body': json_text
    }
    return proxy_response

if os.getenv("SSM_PREFIX"):
    ssm_prefix = os.getenv("SSM_PREFIX")
else:
    logger.error("Environment Var SSM_PREFIX Not Set")

if os.getenv("WEBAUTH_VALIDATE_URL"):
    webauth_validate_url = os.getenv("WEBAUTH_VALIDATE_URL")
else:
    logger.error("Environment Var WEBAUTH_VALIDATE_URL Not Set")

api_private_key = get_ssm_param(f"/{ssm_prefix}/api_private_key", True)
if api_private_key is None:
    logger.error(f"/{ssm_prefix}/api_private_key SSM parameter not found")


def handler(event, context):
    """
    /validate/{ticket}

    Validates a WebAuth CAS ticket, and returns a JWT auth token.
    """

    ticket = event.get('pathParameters', {}).get('ticket')

    if not ticket:
        return _format_response(f'Missing Ticket', 400)

    try:
        logger.info(f"Validating Ticket: {ticket}")
        r = requests.get(f"https://webauth.arizona.edu/webauth/serviceValidate?service=http://localhost:8081/auth&ticket={ticket}")
        logger.info(f"Response Code: {r.status_code}")
        logger.info(r.text)

        root = ET.fromstring(r.text)

        fail_node = root.find(".//{http://www.yale.edu/tp/cas}authenticationFailure")
        if fail_node is not None:
            failure = fail_node.text.strip()
            return _format_response({
                "error": failure
            }, 400)

        netid_node = root.find(".//{http://www.yale.edu/tp/cas}user")
        netid = netid_node.text

        emplid_node = root.find(".//{http://www.yale.edu/tp/cas}emplid")
        emplid = emplid_node.text

    except:
        err_msg = f'Error occurred while validating ticket: {ticket}'
        log_error(err_msg, event, context, traceback.format_exc())
        return _format_response(err_msg, 500)

    payload = {
        "uid": netid,
        "emplid": emplid,
        "iat": datetime.now(timezone.utc),
        'exp': datetime.now(timezone.utc) + timedelta(seconds=3600)
    }

    encoded = jwt.encode(payload, api_private_key, algorithm='RS256')
    encoded = encoded.decode('utf8')
    logger.info(encoded)

    return _format_response(
        "Authentication success",
        ticket=ticket,
        netid=netid,
        emplid=emplid,
        auth_jwt=encoded
    )
