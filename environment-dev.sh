#!/bin/bash
#
# environment-dev.sh

export ACCOUNT_NUMBER_CHECK=237640318024
export REQUIRE_SECRETS_FILE="true"

export KMS_KEY_ARN="arn:aws:kms:us-west-2:#{AWS::AccountId}:key/996a8d2d-73fe-469f-967a-19ba7924c890"
export SMS_PREFIX="serverless-webauth-demo"
export HOSTNAME="localhost:8081"
export URL_BASE="http://$HOSTNAME"
export HOSTNAME_BUILD="local-dev.arizona.edu"
export URL_BASE_BUILD="https://$HOSTNAME_BUILD"
export SERVICE_URL="$URL_BASE/auth"
export WEBAUTH_LOGIN_URL="https://webauth.arizona.edu/webauth/login"
export WEBAUTH_VALIDATE_URL="https://webauth.arizona.edu/webauth/serviceValidate"
